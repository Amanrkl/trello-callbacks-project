/* 
	Problem 6: Write a function that will use the previously written functions to get the following information.
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
const path = require('path')
const boards = require(path.join(__dirname, '/boards.json'));
const callback1 = require(path.join(__dirname, '/callback1.cjs'));
const callback2 = require(path.join(__dirname, '/callback2.cjs'));
const callback3 = require(path.join(__dirname, '/callback3.cjs'));

function callback6(boardName = "Thanos") {

    const timeLimit = Math.ceil(Math.random() * 10);

    setTimeout(() => {
        const boardID = boards.find(board => board.name == boardName)?.id;

        callback1(boardID, (err, boardInfo) => {
            if (err) {
                console.error(err);
            } else {
                console.log(`${boardName} board's information are`);
                console.log(boardInfo);

                callback2(boardID, (err, lists) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log(`${boardName} board's lists are`);
                        console.log(lists);

                        lists.forEach(list => {
                            const listID = list.id;

                            callback3(listID, (err, cards) => {
                                if (err) {
                                    console.error(err);
                                } else {
                                    console.log(`${list.name} list's cards are`);
                                    console.log(cards);

                                }
                            })
                        })
                    }
                })
            }
        })

    }, timeLimit * 1000);
}

module.exports = callback6;