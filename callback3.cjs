/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID 
    that is passed to it from the given data in cards.json. 
    Then pass control back to the code that called it by using a callback function.
*/

const path = require('path');
const fs = require('fs');

const cardsFilePath = path.join(__dirname, '/cards.json');

function callback3(listID, callback) {

    const timeLimit = Math.ceil(Math.random()*10);

    fs.readFile(cardsFilePath, "utf-8", (err, data) => {
        if (err) {
            console.error('Could not read lists data', err);
            callback(err);
        } else {
            if (listID === undefined) {
                const err = new Error("Invalid Input parameter");
                callback(err)
            } else {
                setTimeout(() => {
                    const cards = JSON.parse(data)

                    if (cards.hasOwnProperty(listID)) {
                        callback(null, cards[listID])
                    } else {
                        const err = new Error(`Not found : no ${listID} id found`)
                        callback(err)
                    }
                }, timeLimit * 1000);
            }
        }
    })
}

module.exports = callback3