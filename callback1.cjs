/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID 
    that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

const path = require('path');
const fs = require('fs');

const boardsFilePath = path.join(__dirname, '/boards.json');

function callback1(boardID, callback) {

    const timeLimit = Math.ceil(Math.random()*10);

    fs.readFile(boardsFilePath, "utf-8", (err, data) => {
        if (err) {
            console.error('Could not read boards data', err);
            callback(err);
        } else {
            if (boardID === undefined) {
                const err = new Error("Invalid Input parameter");
                callback(err)
            } else {
                setTimeout(() => {
                    const boards = JSON.parse(data)

                    const searchedBoard = boards.find(board => {
                        return board.id === boardID
                    });
                    if (searchedBoard !== undefined) {
                        callback(null, searchedBoard)
                    } else {
                        const err = new Error(`Not found : no ${boardID} id found`)
                        callback(err)
                    }
                }, timeLimit * 1000);
            }
        }
    })
}

module.exports = callback1