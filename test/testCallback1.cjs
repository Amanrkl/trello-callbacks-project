const callback1 = require('../callback1.cjs');
const boards = require('../boards.json');



function callback(err, data) {
    if (err) {
        console.error(err);
    } else {
        console.log(data);
    }
}

callback1("abc122dc", callback);
callback1("3456", callback);

boards.forEach(board => {
    callback1(board.id, callback)
})