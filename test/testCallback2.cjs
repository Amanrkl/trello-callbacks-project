const callback2 = require('../callback2.cjs');
const lists = require('../lists.json');



function callback(err, data) {
    if (err) {
        console.error(err);
    } else {
        console.log(data);
    }
}
callback2("abc122dc", callback);
callback2("3456", callback);

Object.keys(lists).forEach(listID => {
    callback2(listID, callback)
})