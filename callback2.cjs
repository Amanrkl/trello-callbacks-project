/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID 
    that is passed to it from the given data in lists.json. 
    Then pass control back to the code that called it by using a callback function.
*/

const path = require('path');
const fs = require('fs');

const listsFilePath = path.join(__dirname, '/lists.json');

function callback2(boardID, callback) {

    const timeLimit = Math.ceil(Math.random()*10);

    fs.readFile(listsFilePath, "utf-8", (err, data) => {
        if (err) {
            console.error('Could not read lists data', err);
            callback(err);
        } else {
            if (boardID === undefined) {
                const err = new Error("Invalid Input parameter");
                callback(err)
            } else {
                setTimeout(() => {
                    const lists = JSON.parse(data)

                    if (lists.hasOwnProperty(boardID)) {
                        callback(null, lists[boardID])
                    } else {
                        const err = new Error(`Not found : no ${boardID} id found`)
                        callback(err)
                    }
                }, timeLimit * 1000);
            }
        }
    })
}

module.exports = callback2